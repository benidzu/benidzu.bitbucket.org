
var baseUrl = 'https://rest.ehrscape.com/rest/v1';
var queryUrl = baseUrl + '/query';

var username = "ois.seminar";
var password = "ois4fri";

var imena = ["","Kim","James","Bob"];
var priimki = ["","Kardashian","Corden","Thebuilder"];
var datumirojstva = ["","1965-10-10T00:00:00.000Z", "1963-06-04T00:00:00.000Z", "1955-10-12T00:00:00.000Z"];

var ehridKim = "fe754262-7ad7-4a24-ac80-6569c55bbec3";
var ehridJames = "255a00da-d9bb-45cd-8756-d657654a42dd";
var ehridBob = "ab443ac6-3571-44aa-aa00-e65c8e615657";

var systolics = [100,95,105,110,115,145,140,155,156,143,80,83,75,89,77];
var diastolics = [65,70,73,64,72,92,97,87,90,95,45,51,42,60,48];

var vsebine = [null,null,null,null,null];
var stranskiucinki = [null,null,null,null,null];

var stanje = 0;

const DATA = {
    zdravila: [
        {zdravilo_id: 1, 
         name: "Bystolic"},
        {zdravilo_id: 2, 
         name: "Lisinopril"},
        {zdravilo_id: 3, 
         name: "Atenolol"},
        {zdravilo_id: 4, 
         name: "Midodrine"}
    ]
}

let init = function(){
	try{
    loadMaster(DATA.zdravila);
	}catch(err){
		
	}
}

let loadMaster = function(zdr){
    let ul = document.querySelector('.master-list');
    let df = document.createDocumentFragment();
    zdr.forEach(zdravilo => {
        let li = document.createElement('li');
        li.textContent = zdravilo.name;
        li.className = 'person';
        li.setAttribute('data-key', zdravilo.zdravilo_id);
        li.addEventListener('click', showDetails);
        df.appendChild(li);
    });
    ul.appendChild(df);
}

document.addEventListener('DOMContentLoaded', init);

let showDetails = function(ev){
	$("#podrobnostizdravila").show();
	    let zdravilo_id = ev.target.getAttribute('data-key');
    
    let activePerson;
    DATA.zdravila.forEach(zdravilo=>{
        if(zdravilo.zdravilo_id == zdravilo_id){
            activePerson = zdravilo;
        }
    });
    $("#dejanskavsebina").html("");
    if (vsebine[activePerson.zdravilo_id] != null){
    	document.getElementById('dejanskavsebina').append(vsebine[activePerson.zdravilo_id]);
    }else{
	    var divElement = document.getElementById(activePerson.name).contentWindow.document.getElementById('vsebina');
	    	vsebine[activePerson.zdravilo_id] = divElement;
	    //$("#dejanskavsebina").html("");
		document.getElementById('dejanskavsebina').append(divElement);
    }
    if (stranskiucinki[activePerson.zdravilo_id] != null){
    	document.getElementById('dejanskavsebina').append(stranskiucinki[activePerson.zdravilo_id]);
    }else{
	    var divElement = document.getElementById(activePerson.name).contentWindow.document.getElementById('stranski');
	    	stranskiucinki[activePerson.zdravilo_id] = divElement;
	    //$("#dejanskavsebina").html("");
		document.getElementById('dejanskavsebina').append(divElement);
    }

}

/**
 * Prijava v sistem z privzetim uporabnikom za predmet OIS in pridobitev
 * enolične ID številke za dostop do funkcionalnosti
 * @return enolični identifikator seje za dostop do funkcionalnosti
 */
function getSessionId() {
    var response = $.ajax({
        type: "POST",
        url: baseUrl + "/session?username=" + encodeURIComponent(username) +
                "&password=" + encodeURIComponent(password),
        async: false
    });
    return response.responseJSON.sessionId;
}



/**
 * Generator podatkov za novega pacienta, ki bo uporabljal aplikacijo. Pri
 * generiranju podatkov je potrebno najprej kreirati novega pacienta z
 * določenimi osebnimi podatki (ime, priimek in datum rojstva) ter za njega
 * shraniti nekaj podatkov o vitalnih znakih.
 * @param stPacienta zaporedna številka pacienta (1, 2 ali 3)
 * @return ehrId generiranega pacienta
 */
function generirajPodatke(stPacienta) {
  var ehrId = "";
    
  	var sessionId = getSessionId();

	

		$.ajaxSetup({
		    headers: {"Ehr-Session": sessionId}
		});
		$.ajax({
		    url: baseUrl + "/ehr",
		    type: 'POST',
		    success: function (data) {
		        var ehrId = data.ehrId;
		        
		        if(stPacienta == 1){
	                ehridKim = ehrId;
	            }
	            else if(stPacienta == 2){
	                ehridJames = ehrId;
	            }
	            else{
	                ehridBob = ehrId;
	            }
		        
		        var partyData = {
		            firstNames: imena[stPacienta],
		            lastNames: priimki[stPacienta],
		            dateOfBirth: datumirojstva[stPacienta],
		            partyAdditionalInfo: [{key: "ehrId", value: ehrId}]
		        };
		        $.ajax({
		            url: baseUrl + "/demographics/party",
		            type: 'POST',
		            contentType: 'application/json',
		            data: JSON.stringify(partyData),
		            success: function (party) {
		                if (party.action == 'CREATE') {
		                	
			                	for (var i = 0; i<5; i++){
	                        		ustvariTestne(stPacienta, i, ehrId, sessionId);
	                        	}
		                	
		                    console.log("Uspešno kreiran EHR '" + ehrId + "'.");
		                    //console.log("Wat");
		                    $("#glavca").append("<span class='obvestilo " +
                          "label label-success fade-in'>Uspešno kreiran EHR '" +
                          ehrId +"' za " + imena[stPacienta] + " " + priimki[stPacienta] +  ".</br></span>");
                          
		                    $("#preberiEHRid").val(ehrId);
		                	}
		                },
		            error: function(err) {
		            	$("#glavca").html("<span class='obvestilo label " +
                    "label-danger fade-in'>Napaka '" +
                    JSON.parse(err.responseText).userMessage + "'!");
		            }
		        });
		    }
		});
	

  return ehrId;
}

function pocistihtml(){
	$("#glavca").html("");
}

function ustvariTestne(stPacienta, i, ehrId, sessionId){
    //var sessionId = getSessionId();


	var sistolicniKrvniTlak = systolics[(stPacienta-1)*5+i];
	var diastolicniKrvniTlak = diastolics[(stPacienta-1)*5+i];
	var telesnaTeza = 66;


	
		$.ajaxSetup({
		    headers: {"Ehr-Session": sessionId}
		});
		var podatki = {
			// Struktura predloge je na voljo na naslednjem spletnem naslovu:
      // https://rest.ehrscape.com/rest/v1/template/Vital%20Signs/example
		    "ctx/language": "en",
		    "ctx/territory": "SI",
		    "ctx/time": "2018-03-02T"+(10+2*i)+":"+"00"+"Z",
		    "vital_signs/blood_pressure/any_event/systolic": sistolicniKrvniTlak,
		    "vital_signs/blood_pressure/any_event/diastolic": diastolicniKrvniTlak,
		    "vital_signs/body_weight/any_event/body_weight": telesnaTeza,

		    
		};
		var parametriZahteve = {
		    ehrId: ehrId,
		    templateId: 'Vital Signs',
		    format: 'FLAT',
		    committer: 'DrStrange'
		};
		$.ajax({
		    url: baseUrl + "/composition?" + $.param(parametriZahteve),
		    type: 'POST',
		    contentType: 'application/json',
		    data: JSON.stringify(podatki),
		    success: function (res) {
		        /*$("#dodajMeritveVitalnihZnakovSporocilo").html(
              "<span class='obvestilo label label-success fade-in'>" +
              res.meta.href + ".</span>");*/
              console.log("Uspesno dodane zahteve.");
		    },
		    error: function(err) {
		    	/*$("#dodajMeritveVitalnihZnakovSporocilo").html(
            "<span class='obvestilo label label-danger fade-in'>Napaka '" +
            JSON.parse(err.responseText).userMessage + "'!");*/
            console.log("Napaka.");
		    }
		});
	
}


function izberiEhrId() {
	
	var bolnik = $('#opcije').val();
	var polje = document.getElementById("meritveEHRid");
	if (bolnik == "1"){
        polje.value = ehridKim;
	}
	if (bolnik == "2"){
        polje.value = ehridJames;	
	}
	if (bolnik == "3"){
        polje.value = ehridBob;	
    }
    
}

function kreirajEHRzaBolnika() {
	var sessionId = getSessionId();

	var ime = $("#kreirajIme").val();
	var priimek = $("#kreirajPriimek").val();
  var datumRojstva = $("#kreirajDatumRojstva").val() + "T00:00:00.000Z";

	if (!ime || !priimek || !datumRojstva || ime.trim().length == 0 ||
      priimek.trim().length == 0 || datumRojstva.trim().length == 0) {
		$("#glavca").html("<span class='obvestilo label " +
      "label-warning fade-in'>Prosim vnesite zahtevane podatke!</span>");
	} else {
		$.ajaxSetup({
		    headers: {"Ehr-Session": sessionId}
		});
		$.ajax({
		    url: baseUrl + "/ehr",
		    type: 'POST',
		    success: function (data) {
		        var ehrId = data.ehrId;
		        var partyData = {
		            firstNames: ime,
		            lastNames: priimek,
		            dateOfBirth: datumRojstva,
		            partyAdditionalInfo: [{key: "ehrId", value: ehrId}]
		        };
		        $.ajax({
		            url: baseUrl + "/demographics/party",
		            type: 'POST',
		            contentType: 'application/json',
		            data: JSON.stringify(partyData),
		            success: function (party) {
		                if (party.action == 'CREATE') {
		                    $("#glavca").html("<span class='obvestilo " +
                          "label label-success fade-in'>Uspešno kreiran EHR '" +
                          ehrId + "'.</span>");
		                    $("#meritveEHRid").val(ehrId);
		                }
		            },
		            error: function(err) {
		            	$("#glavca").html("<span class='obvestilo label " +
                    "label-danger fade-in'>Napaka '" +
                    JSON.parse(err.responseText).userMessage + "'!");
		            }
		        });
		    }
		});
	}
}

function dodajMeritevKrvnegaTlaka() {
	var sessionId = getSessionId();

	var ehrId = $("#dodajVitalnoEHR").val();
	var datumInUra = $("#dodajVitalnoDatumInUra").val();
	var sistolicniKrvniTlak = $("#dodajVitalnoKrvniTlakSistolicni").val();
	var diastolicniKrvniTlak = $("#dodajVitalnoKrvniTlakDiastolicni").val();

	if (!ehrId || ehrId.trim().length == 0) {
		$("#glavca").html("<span class='obvestilo " +
      "label label-warning fade-in'>Prosim vnesite zahtevane podatke!</span>");
	} else {
		$.ajaxSetup({
		    headers: {"Ehr-Session": sessionId}
		});
		var podatki = {
			// Struktura predloge je na voljo na naslednjem spletnem naslovu:
      // https://rest.ehrscape.com/rest/v1/template/Vital%20Signs/example
		    "ctx/language": "en",
		    "ctx/territory": "SI",
		    "ctx/time": datumInUra,
		    "vital_signs/blood_pressure/any_event/systolic": sistolicniKrvniTlak,
		    "vital_signs/blood_pressure/any_event/diastolic": diastolicniKrvniTlak,
		};
		var parametriZahteve = {
		    ehrId: ehrId,
		    templateId: 'Vital Signs',
		    format: 'FLAT',
		    committer: 'DrStrange'
		};
		$.ajax({
		    url: baseUrl + "/composition?" + $.param(parametriZahteve),
		    type: 'POST',
		    contentType: 'application/json',
		    data: JSON.stringify(podatki),
		    success: function (res) {
		        $("#glavca").html(
              "<span class='obvestilo label label-success fade-in'>" +
              "Podatki uspešno vnešeni!" + ".</span>");
		    },
		    error: function(err) {
		    	$("#glavca").html(
            "<span class='obvestilo label label-danger fade-in'>Napaka '" +
            JSON.parse(err.responseText).userMessage + "'!");
		    }
		});
	}
}

function preberiMeritve() {
	$("#panelgraf").show();
	var sessionId = getSessionId();

	var ehrId = $("#meritveEHRid").val();


	if (!ehrId || ehrId.trim().length == 0) {
		$("#glavca").html("<span class='obvestilo " +
      "label label-warning fade-in'>Prosim vnesite zahtevan podatek!");
	} else {
		$.ajax({
			url: baseUrl + "/demographics/ehr/" + ehrId + "/party",
	    	type: 'GET',
	    	headers: {"Ehr-Session": sessionId},
	    	success: function (data) {
				var party = data.party;
				$("#rezultatMeritveVitalnihZnakov").html("<br/><span>Pridobivanje " +
          "podatkov za <b>'" + "krvni tlak" + "'</b> bolnika <b>'" + party.firstNames +
          " " + party.lastNames + "'</b>.</span><br/><br/>");
				$.ajax({
				    url: baseUrl + "/view/" + ehrId + "/" + "blood_pressure",
				    type: 'GET',
				    headers: {"Ehr-Session": sessionId},
				    success: function (res) {
				    	if (res.length > 0) {
				    		var stPodatkov = res.length-1;
        					var podatki = new Array(res.length);
					    	var results = "<table class='table table-striped " +
                "table-hover'><tr><th>Datum in ura</th>" +
                "<th class='text-center'>Sistolični tlak</th><th class='text-right'>Diastolični tlak</th></tr>";
					        for (var i in res) {
					        	 podatki[i] = {"category": (res[stPodatkov-i].time).substring(5,10)+" ob "+(res[stPodatkov-i].time).substring(11,16),
									"column-1": res[stPodatkov-i].systolic,
									"column-2": res[stPodatkov-i].diastolic
		                         };
					        	
					            results += "<tr><td>" + res[i].time +
                      "</td><td class='text-center'>" + res[i].systolic + "</td>" +
                      "<td class = 'text-right'>" + res[i].diastolic + "</td>" +
                      "</tr>";
                    	
                    			if(res[stPodatkov-i].systolic < 90 || res[stPodatkov-i].diastolic < 61){
				                    stanje = 1;
				                }
				                else if(res[stPodatkov-i].systolic > 130 || res[stPodatkov-i].diastolic > 80){
				                    stanje = 2;
				                }else{
				                	stanje = 0;
				                }
					        }
					        results += "</table>";
					        $("#rezultatMeritveVitalnihZnakov").append(results);
					        
					        //------graf
					        AmCharts.makeChart("chartdiv",
							{
								"type": "serial",
								"categoryField": "category",
								"colors": [
			                		"#FF00FF",
			                		"#0000FF",
			                		"#B0DE09",
			                		"#0D8ECF",
			                		"#2A0CD0",
			                		"#CD0D74",
			                		"#CC0000",
			                		"#00CC00",
			                		"#0000CC",
			                		"#DDDDDD",
			                		"#999999",
			                		"#333333",
			                		"#990000"
			            	    ],
								"startDuration": 0,
								"categoryAxis": {
									"gridPosition": "start"
								},
								"trendLines": [],
								"graphs": [
									{
										"balloonText": "[[title]] dne [[category]], vrednost [[value]]",
										"bullet": "round",
										"bulletSize": 12,
										"lineThickness": 7,
										"id": "AmGraph-1",
										"title": "sistolični tlak",
										"valueField": "column-1"
									},
									{
										"balloonText": "[[title]] ob [[category]], vrednost [[value]]",
										"bullet": "square",
										"bulletSize": 12,
										"lineThickness": 7,
										"id": "AmGraph-2",
										"title": "diastolični tlak",
										"valueField": "column-2"
									}
								],
								"guides": [{
			                        "value": 90,
			                        "toValue": 130,
			                        "lineColor": "#C0C0C0",
			                        "lineAlpha": 1,
			                        "fillAlpha": 0.2,
			                        "fillColor": "#800080",
			                        "dashLength": 2,
			                        "inside": true,
			                        "label": "normalen sistolični tlak",
			                        "valueAxis": "ValueAxis-1"
			                    },
			                    {
			                        "value": 60,
			                        "toValue": 80,
			                        "lineColor": "#0000FF",
			                        "lineAlpha": 1,
			                        "fillAlpha": 0.2,
			                        "fillColor": "#008080",
			                        "dashLength": 2,
			                        "inside": true,
			                        "label": "normalen diastolični tlak",
			                        "valueAxis": "ValueAxis-1"
			                    }],
								"valueAxes": [
									{
										"id": "ValueAxis-1",
										"title": "mm Hg",
										"maximum": 170,
										"minimum": 0
									}
								],
								"allLabels": [],
								"balloon": {},
								"legend": {
									"enabled": true,
									"useGraphSettings": true
								},
								"titles": [
									{
										"id": "Title-1",
										"size": 15,
										"text": "Krvni Tlak"
									}
								],
								"dataProvider": podatki
							}
						);
					        
					    if (stanje == 0){
					    	$("#rezultatm").html("<p> <h4><b>Sporočilo doktorja</b> :</h4></p> <p>Vaš krvni tlak je v mejah normale. :)  </p>")
					    }
					    if (stanje == 1){
					    	$("#rezultatm").html("<p> <h4><b>Sporočilo doktorja</b> :</h4></p> <p>Vaš krvni tlak je <b>nenavadno nizek</b>. Razmislite o uporabi zdravila Midodrine </p>")
					    }
					    if (stanje == 2){
					    	$("#rezultatm").html("<p> <h4><b>Sporočilo doktorja</b> :</h4></p> <p>Vaš krvni tlak je <b>nenavadno visok</b>. Razmislite o uporabi zdravila Atenolol </p>")
					    }
					        
				    	} else {
				    		$("#preberiMeritveVitalnihZnakovSporocilo").html(
                "<span class='obvestilo label label-warning fade-in'>" +
                "Ni podatkov!</span>");
				    	}
				    },
				    error: function() {
				    	$("#preberiMeritveVitalnihZnakovSporocilo").html(
              "<span class='obvestilo label label-danger fade-in'>Napaka '" +
              JSON.parse(err.responseText).userMessage + "'!");
				    }
				});
	    	},
	    	error: function(err) {
	    		$("#preberiMeritveVitalnihZnakovSporocilo").html(
            "<span class='obvestilo label label-danger fade-in'>Napaka '" +
            JSON.parse(err.responseText).userMessage + "'!");
	    	}
		});
	}
}


// TODO: Tukaj implementirate funkcionalnost, ki jo podpira vaša aplikacija
